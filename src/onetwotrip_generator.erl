-module(onetwotrip_generator).
-behaviour(gen_server).

-define(SERVER, ?MODULE).

-include("../include/properties.hrl").

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

start_link() -> gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

init(_Args) ->
  {ok, C} = eredis:start_link(),
  erlang:send_after(?GENERATOR_TIMEOUT, self(), generate),
  {ok, {C, 0}}.

handle_call(_Request, _From, State) -> {reply, ok, State}.

handle_cast(_Msg, State) -> {noreply, State}.

handle_info(generate, {C, N} = _State) ->
  erlang:send_after(?GENERATOR_TIMEOUT, self(), generate),
  {ok, _} = eredis:q(C, ["RPUSH", ?QUEUE, N]),
  lager:info("Generated message: ~p", [N]),
  {noreply, {C, N + 1}};

handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.
