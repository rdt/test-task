-module(onetwotrip_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

start_link() -> supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  ChildSpec = case os:getenv("TYPE") of
    "generator" -> [?CHILD(onetwotrip_generator, worker)];
    "client"    -> [?CHILD(onetwotrip_client, worker)];
    "cleaner"   -> onetwotrip_cleaner:clean(), halt(0);
    Type        -> io:format("Undefined application type: ~p~n", [Type]), halt(1)
  end,
  {ok, {{one_for_one, 5, 10}, ChildSpec}}.
