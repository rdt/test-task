-module(onetwotrip_client).
-behaviour(gen_server).

-define(SERVER, ?MODULE).

-include("../include/properties.hrl").

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

start_link() -> gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

init(_Args) ->
  {ok, C} = eredis:start_link(),
  erlang:send_after(?CLIENT_TIMEOUT, self(), pull),
  {ok, C}.

handle_call(_Request, _From, State) -> {reply, ok, State}.

handle_cast(_Msg, State) -> {noreply, State}.

handle_info(pull, C) ->
  lager:info("Pulled message from : ~p", [?QUEUE]),
  {ok, Value} = eredis:q(C, ["LPOP", ?QUEUE]),
  lager:info("Popped message: ~p", [Value]),
  case onetwotrip_handler:handle(Value) of
    error ->
      lager:error("Error handling message: ~p, pushed to error queue: ~p", [Value, ?QUEUE_ERRORS]),
      {ok, _} = eredis:q(C, ["RPUSH", ?QUEUE_ERRORS, Value]);
    ok -> lager:info("Handled message: ~p", [Value])
  end,
  erlang:send_after(?CLIENT_TIMEOUT, self(), pull),
  {noreply, C};

handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.
