-module(onetwotrip_cleaner).
-export([clean/0]).

-include("../include/properties.hrl").

clean() ->
  {ok, C} = eredis:start_link(),
  {ok, Values} = eredis:q(C, ["LRANGE", ?QUEUE_ERRORS, 0, -1]),
  {ok, _} = eredis:q(C, ["DEL", ?QUEUE_ERRORS]),
  io:format("Messages : ~p~n", [Values]),
  ok.