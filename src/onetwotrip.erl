-module(onetwotrip).
-export([start/0]).

start() ->
  {ok, _} = application:ensure_all_started(lager),
  {ok, _} = application:ensure_all_started(eredis),
  {ok, _} = application:ensure_all_started(sync),
  ok = application:start(onetwotrip).