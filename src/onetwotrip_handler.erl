-module(onetwotrip_handler).
-export([handle/1]).

handle(undefined) -> ok;
handle(_Message) ->
  timer:sleep(round(random:uniform() * 1000)),
  case random:uniform() > 0.85 of
    true -> error;
    false -> ok
  end.