REBAR = ./rebar
APP   = onetwotrip
RUN   = erl -oldshell -pa deps/*/ebin -pa $(PWD)/ebin -s $(APP) start

all: deps compile test client

deps:
	$(REBAR) get-deps

compile:
	$(REBAR) compile

clean:
	$(REBAR) clean

test:
	$(REBAR) eu

client:
	TYPE=client $(RUN)

generator:
	TYPE=generator $(RUN)

cleaner:
	TYPE=cleaner $(RUN)

.PHONY: deps compile clean test client generator cleaner
